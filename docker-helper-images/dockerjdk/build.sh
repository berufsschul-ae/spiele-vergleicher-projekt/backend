#!/bin/sh bash

TAG="1.0"
REPOSITORY="registry.gitlab.com/berufsschul-ae/spiele-vergleicher/"
IMAGE_NAME="docker-openjdk11"

docker build -t $REPOSITORY$IMAGE_NAME:$TAG .
docker build -t $REPOSITORY$IMAGE_NAME:latest .

docker push $REPOSITORY$IMAGE_NAME:$TAG
docker push $REPOSITORY$IMAGE_NAME:latest
