package sv.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

public class UserTest {

    @Test 
    public void shouldSetNewGameListByClearingTheOldOne() {
        var user = new User("username");
        var game = new Game();

        assertEquals(0, user.getGames().size());
        user.setGames(List.of(game, game));
        assertEquals(2, user.getGames().size());
        user.setGames(List.of(game));
        assertEquals(1, user.getGames().size());
    }
}