package sv.logger;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class ConsoleLoggerTest {

    @Test
    public void shouldFormatInfoLog() {
        LoggerConfig loggerConfig = new LoggerConfig.LoggerConfigBuilder().setDateTime(new MockDateTime()).build();
        MockLogWriter mockLogWriter = new MockLogWriter();
        Logger logger = new Logger(loggerConfig, mockLogWriter);
        logger.info("test");
        System.out.println(mockLogWriter.getMessage());
        assertEquals("\u001B[37m 07:00:00 INFO: test \u001B[0m", mockLogWriter.getMessage());

    }


    private static class MockDateTime implements DateTime {

        @Override
        public LocalDateTime now() {
            return LocalDateTime.of(2000, Month.APRIL, 1, 7, 0);
        }
    }

    private static class MockLogWriter implements LogWriter {

        private String message;

        @Override
        public void write(String logMessage) {
            this.message = logMessage;
        }

        public String getMessage() {
            return message;
        }
    }

}