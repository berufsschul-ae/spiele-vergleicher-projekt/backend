package sv.service;

import org.springframework.stereotype.Service;
import sv.logger.Logger;
import sv.logger.LoggerFactory;
import sv.model.Lobby;
import sv.model.User;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class LobbyService {

    private final Map<UUID, Lobby> lobbies = new ConcurrentHashMap<UUID, Lobby>();
    private final Logger log = LoggerFactory.getConsoleLogger();

    public boolean contains(UUID uuid) {
        return lobbies.containsKey(uuid);
    }

    public Lobby findBy(UUID uuid) {
        return lobbies.get(uuid);
    }

    public boolean add(Lobby lobby) {
        Objects.requireNonNull(lobby, "The lobby is null and this is not allowed");
        if (lobbies.containsKey(lobby.getUuid()))
            return false;
        lobbies.put(lobby.getUuid(), lobby);
        log.info("new lobby added. Total count now: " + lobbies.size());
        return true;
    }

    public void remove(UUID uuid) {
        if (!lobbies.containsKey(uuid))
            return;
        lobbies.remove(uuid);
    }

    public boolean checkIfAllUsersAreReady(Lobby lobby) {
        List<User> users = lobby.getAllUsers();

        boolean allUsersReady = true;

        for (User user : users) {
            if (!user.isReady()) {
                allUsersReady = false;
            }
        }

        return allUsersReady;
    }

}