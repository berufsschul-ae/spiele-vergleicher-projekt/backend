package sv.logger;

public class ConsoleLogger implements LogWriter {
    @Override
    public void write(String logMessage) {
        System.out.println(logMessage);
    }
}
