package sv.logger;

public enum LogLevel {

    ERROR("ERROR") {
    },
    INFO("INFO") {

    },
    DEBUG("DEBUG") {

    };

    private final String logLevel;

    LogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public String getLogLevel() {
        return logLevel;
    }
}
