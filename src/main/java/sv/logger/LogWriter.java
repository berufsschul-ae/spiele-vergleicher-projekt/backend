package sv.logger;

public interface LogWriter {

    void write(String logMessage);
}
