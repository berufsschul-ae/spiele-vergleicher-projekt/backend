package sv.logger;

import java.time.LocalDateTime;

public class Logger {

    private final LoggerConfig loggerConfig;
    private final LogWriter logWriter;
    private final static String RESET_COLOR = Color.ANSI_RESET.getColorCode();

    public Logger(LoggerConfig loggerConfig, LogWriter logWriter) {
        this.loggerConfig = loggerConfig;
        this.logWriter = logWriter;
    }

    public void info(String s) {
        logWriter.write(logFormat(loggerConfig.getInfoColor(), LogLevel.INFO.getLogLevel(), s));
    }

    public void debug(String s) {
        logWriter.write(logFormat(loggerConfig.getDebugColor(), LogLevel.DEBUG.getLogLevel(), s));
    }

    public void error(String s) {
        logWriter.write(logFormat(loggerConfig.getErrorColor(), LogLevel.ERROR.getLogLevel(), s));
    }

    private String logFormat(String color, String logLevel, String message) {
        return String.format("%s %s %s: %s %s", color, loggerConfig.getDateTime().now().format(loggerConfig.getDateTimeFormatter()), logLevel, message, RESET_COLOR);
    }
}
