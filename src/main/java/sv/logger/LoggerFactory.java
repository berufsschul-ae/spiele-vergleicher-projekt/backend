package sv.logger;

import java.io.File;

public class LoggerFactory {

    private static final LoggerConfig DEFAULT_LOGGER_CONFIG = new LoggerConfig.LoggerConfigBuilder().build();

    public static Logger getConsoleLogger() {
        return new Logger(DEFAULT_LOGGER_CONFIG, new ConsoleLogger());
    }

    public static Logger getFileLogger(File file) {
        return new Logger(DEFAULT_LOGGER_CONFIG, new FileLogger(file));
    }
}
