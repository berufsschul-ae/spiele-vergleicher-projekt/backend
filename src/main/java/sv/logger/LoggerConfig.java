package sv.logger;

import java.time.format.DateTimeFormatter;

public class LoggerConfig {

    private final DateTimeFormatter dateFormat;
    private final DateTime dateTime;
    private final String errorColor;
    private final String debugColor;
    private final String infoColor;

    public LoggerConfig(DateTimeFormatter dateFormat, DateTime dateTime, String errorColor, String debugColor, String infoColor) {
        this.dateFormat = dateFormat;
        this.dateTime = dateTime;
        this.errorColor = errorColor;
        this.debugColor = debugColor;
        this.infoColor = infoColor;
    }

    public DateTimeFormatter getDateTimeFormatter() {
        return this.dateFormat;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public String getErrorColor() {
        return this.errorColor;
    }

    public String getDebugColor() {
        return debugColor;
    }

    public String getInfoColor() {
        return infoColor;
    }

    public static class LoggerConfigBuilder {

        private DateTimeFormatter dateFormat = DateTimeFormatter.ISO_TIME;
        private DateTime dateTime = new LocalDateTimeWrapper();
        private String errorColor = Color.ANSI_RED.getColorCode();
        private String debugColor = Color.ANSI_YELLOW.getColorCode();
        private String infoColor = Color.ANSI_WHITE.getColorCode();

        public LoggerConfigBuilder setDateTimeFormatter(DateTimeFormatter dateFormat) {
            this.dateFormat = dateFormat;
            return this;
        }

        public LoggerConfigBuilder setDateTime(DateTime dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public LoggerConfigBuilder setErrorColor(String errorColor) {
            this.errorColor = errorColor;
            return this;
        }

        public LoggerConfigBuilder setDebugColor(String debugColor) {
            this.debugColor = debugColor;
            return this;
        }

        public LoggerConfigBuilder setInfoColor(String infoColor) {
            this.infoColor = infoColor;
            return this;
        }

        public LoggerConfig build() {
            return new LoggerConfig(this.dateFormat, this.dateTime, this.errorColor, this.debugColor, this.infoColor);
        }
    }
}
