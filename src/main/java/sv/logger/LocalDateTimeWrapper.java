package sv.logger;

import java.time.LocalDateTime;

public class LocalDateTimeWrapper implements DateTime {
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
