package sv.logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public interface DateTime {
    LocalDateTime now();
}
