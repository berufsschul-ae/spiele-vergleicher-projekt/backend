package sv.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileLogger implements LogWriter {

    private final File file;

    public FileLogger(File file) {
        this.file = file;
    }

    @Override
    public void write(String logMessage) {
        try {
            new FileWriter(file).write(logMessage);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
