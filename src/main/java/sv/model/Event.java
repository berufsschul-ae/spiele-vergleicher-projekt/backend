package sv.model;

public enum Event {

    CONNECT, READY_STATE_CHANGE, ALL_READY, DISCONNECT, ERROR

}