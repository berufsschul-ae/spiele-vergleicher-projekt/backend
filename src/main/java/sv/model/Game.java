package sv.model;

import lombok.Data;

@Data
public class Game {
    private long id;
    private String title;
    private String description;

    public Game() {}
}
