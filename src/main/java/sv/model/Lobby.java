package sv.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.common.collect.ImmutableList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Data
public class Lobby {

    private final UUID uuid;
    private final LocalDate createTime;
    private List<Game> commonGames;

    @Getter(value = AccessLevel.NONE)
    private final Map<UUID, User> users;

    public Lobby() {
        this.uuid = UUID.randomUUID();
        this.createTime = LocalDate.now();
        this.users = new ConcurrentHashMap<UUID, User>();
        this.commonGames = new CopyOnWriteArrayList<Game>();
    }

    public boolean containsUser(final UUID uuid) {
        return users.containsKey(uuid);
    }

    public boolean add(final User user) {
        Objects.requireNonNull(user, "The provided user is null and can not be added to a lobby!");
        if (containsUser(user.getUuid()))
            return false;
        users.put(user.getUuid(), user);
        return true;
    }

    public User getUserById(final UUID uuid) {
        return users.get(uuid);
    }

    public ImmutableList<User> getAllUsers() {
        return ImmutableList.copyOf(users.values());
    }

    public void setCommonGames(List<Game> gameList) {
        commonGames.clear();
        commonGames.addAll(gameList);
    }
}